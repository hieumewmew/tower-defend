package toangLaRo;
public class Coordinate
{
    // Every object will have an x, y integer.
	
	public int x, y;

	public Coordinate(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	/**
	 * Get X coordinate
	 * 
	 * @return X coordinate
	 */
	public int getX()
	{
		return this.x; 
	}
	
	/**
	 * Get Y coordinate
	 * 
	 * @return Y coordinate
	 */
	public int getY()
	{
		return this.y; 
	}
	
	public String toString()
	{
		return ("" + x + " " + y);
	}

}